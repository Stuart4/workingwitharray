/**
 * Created by jake on 6/24/14.
 */
//import java.util.Arrays;

public class WorkingWithArray {

	final int MAX_LENGTH = 55;

	public static void main(String[] args){
		WorkingWithArray me = new WorkingWithArray();
//		WordList word = new WordList(args);
		me.printHistogram(me.computeHistogram(args));


	}

	public int[] computeHistogram(String[] words){
		int[] hist = new int[MAX_LENGTH];


		for (String n : words){

			hist[n.length()]++;
		}
		return hist;
	}

	public void printHistogram(int[] hist){
		for (int i = 0; i < hist.length; i++){

			if (hist[i] != 0) {
				System.out.printf("Words of length %d = %d\n", i, hist[i]);
			}
		}
	}
}